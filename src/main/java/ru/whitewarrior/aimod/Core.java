package ru.whitewarrior.aimod;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import ru.whitewarrior.aimod.handler.AiEventHandler;

import static ru.whitewarrior.aimod.Core.MOD_ID;

@Mod(modid = MOD_ID, name = "Ai test mod", version = "1.0")
public class Core {
    public static final String MOD_ID = "aitest";

    @Mod.Instance(MOD_ID)
    public static Core INSTANCE;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event){
        MinecraftForge.EVENT_BUS.register(new AiEventHandler());
    }

}
