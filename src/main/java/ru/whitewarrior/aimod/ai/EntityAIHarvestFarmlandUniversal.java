package ru.whitewarrior.aimod.ai;

import net.minecraft.block.Block;
import net.minecraft.block.BlockCrops;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIMoveToBlock;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.whitewarrior.aimod.utils.HarvestUtil;
import ru.whitewarrior.aimod.utils.InventoryUtil;
import ru.whitewarrior.aimod.utils.QueueInventory;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static ru.whitewarrior.aimod.utils.InventoryUtil.INV_TAG_NAME;

public class EntityAIHarvestFarmlandUniversal extends EntityAIMoveToBlock {
    private final EntityLiving living;
    private Deque<BlockPos> farmlands;

    public EntityAIHarvestFarmlandUniversal(EntityCreature entity, double speedIn) {
        super(entity, speedIn, 16);
        this.living = entity;
    }

    @Override
    public boolean isInterruptible() {
        return false;
    }

    private void saveEntityInventory(QueueInventory basic) {
        NBTTagCompound serialized = InventoryUtil.serializeInventory(basic);
        living.getEntityData().setTag(INV_TAG_NAME, serialized);
    }

    public boolean shouldExecute() {
        if (runDelay <= 0)
            if (!net.minecraftforge.event.ForgeEventFactory.getMobGriefingEvent(living.world, living))
                return false;

        if (farmlands != null && !farmlands.isEmpty()) {
            destinationBlock = farmlands.poll();
            return true;
        } else if (farmlands != null && HarvestUtil.getHarvestsCount(InventoryUtil.loadEntityInventory(creature)) > 0 && getIsAboveDestination())
            return false;

        if (runDelay > 0) {
            runDelay--;
            return false;
        } else {
            boolean result = super.shouldExecute();
            runDelay = result ? 30 : 0;
            return result;
        }
    }

    /**
     * Returns whether an in-progress EntityAIBase should continue executing
     */
    public boolean shouldContinueExecuting() {
        return !farmlands.isEmpty() || !getIsAboveDestination();
    }

    /**
     * Keep ticking a continuous task that has already been started
     */
    public void updateTask() {
        double centerX = destinationBlock.getX() + 0.5D;
        double y = destinationBlock.getY() + 1;
        double centerZ = destinationBlock.getZ() + 0.5D;

        if (creature.getDistanceSqToCenter(destinationBlock.up()) > 1.0D) {
            isAboveDestination = false;
            timeoutCounter++;

            if (timeoutCounter % 12 == 0)
                creature.getNavigator().tryMoveToXYZ(centerX, y, centerZ, movementSpeed);
        } else {
            isAboveDestination = true;
            --timeoutCounter;
        }

        living.getLookHelper().setLookPosition(centerX, y, centerZ, 10.0F, living.getVerticalFaceSpeed());
        QueueInventory inventory = InventoryUtil.loadEntityInventory(creature);


        if (getIsAboveDestination()) {
            World world = living.world;
            BlockPos blockpos = destinationBlock.up();
            IBlockState iblockstate = world.getBlockState(blockpos);
            Block block = iblockstate.getBlock();

            if (block instanceof BlockCrops && ((BlockCrops) block).isMaxAge(iblockstate)) {
                NonNullList<ItemStack> ret = NonNullList.create();
                block.getDrops(ret, world, blockpos, iblockstate, 0);
                world.destroyBlock(blockpos, false);

                for (ItemStack stack : ret)
                    if (HarvestUtil.isHarvestingItem(stack))
                        inventory.addItem(stack);
                saveEntityInventory(inventory);
            }

            if (farmlands != null && !farmlands.isEmpty()) {
                destinationBlock = farmlands.poll();
                isAboveDestination = false;
            }

            runDelay = 0;
        }
    }


    /**
     * @return 1 if block in input position is farmland with harvest
     * 0 if block in input position is farmland without harvest,
     * -1 if block in input position isn't farmland
     */
    private int getBlockFarmlandStatus(World worldIn, BlockPos pos) {
        if (worldIn.getBlockState(pos).getBlock() != Blocks.FARMLAND)
            return -1;

        pos = pos.up();
        IBlockState iblockstate = worldIn.getBlockState(pos);
        Block block = iblockstate.getBlock();
        boolean b = block instanceof BlockCrops && ((BlockCrops) block).isMaxAge(iblockstate);
        return b ? 1 : 0;
    }

    @Nullable
    private Map<BlockPos, Boolean> getFarmBlocks(World worldIn, BlockPos pos) {
        if (worldIn.getBlockState(pos).getBlock() != Blocks.FARMLAND)
            return null;

        if(worldIn.getBlockState(pos.east()).getBlock() == Blocks.FARMLAND)
            return null;
        if (worldIn.getBlockState(pos.south()).getBlock() == Blocks.FARMLAND)
            return null;


        Map<BlockPos, Boolean> farmlands = null;
        boolean isFarmUseful = false;
        int maxColumns = 7;
        //in case of corrupted farm
        for (int row = 0; row < 7; row++) {
            for (int column = 0; column < maxColumns; column++) {
                BlockPos next = pos.north(row).west(column);
                int status = getBlockFarmlandStatus(worldIn, next);
                if (status == -1) {
                    maxColumns = column;
                    break;
                } else {
                    if (status == 1)
                        isFarmUseful = true;

                    if (farmlands == null)
                        farmlands = new LinkedHashMap<>();
                    farmlands.put(next, status == 1);
                }
            }
        }

        return isFarmUseful ? farmlands : null;
    }

    protected boolean shouldMoveTo(@NotNull World worldIn, @NotNull BlockPos pos) {
        Map<BlockPos, Boolean> farmlands = getFarmBlocks(worldIn, pos);
        if (farmlands != null) {
            this.farmlands = farmlands.entrySet().stream()
                    .filter(Map.Entry::getValue).map(Map.Entry::getKey)
                    .collect(Collectors.toCollection(ArrayDeque::new));

            return true;
        } else return false;
    }
}