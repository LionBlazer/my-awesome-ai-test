package ru.whitewarrior.aimod.ai;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDoor;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.pathfinding.Path;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.village.Village;
import ru.whitewarrior.aimod.utils.HarvestUtil;
import ru.whitewarrior.aimod.utils.InventoryUtil;
import ru.whitewarrior.aimod.utils.QueueInventory;

import static ru.whitewarrior.aimod.utils.InventoryUtil.INV_TAG_NAME;

public class EntityAIMoveToNextDoor extends EntityAIBase {
    private final EntityCreature creature;
    private final double movementSpeed;
    private int runDelay;
    private int timeoutCounter;

    private BlockPos destinationBlock = BlockPos.ORIGIN;
    private BlockPos destinationDoor = BlockPos.ORIGIN;
    private Village localVillage;
    private int currentDoor;
    private int failureStreak;

    private QueueInventory localInv;

    public EntityAIMoveToNextDoor(EntityCreature creature, double movementSpeed) {
        this.creature = creature;
        this.movementSpeed = movementSpeed;
        setMutexBits(5);
    }

    private void saveEntityInventory(QueueInventory basic) {
        NBTTagCompound serialized = InventoryUtil.serializeInventory(basic);
        creature.getEntityData().setTag(INV_TAG_NAME, serialized);
    }

    public boolean shouldExecute() {
        if (runDelay > 0) {
            --runDelay;
            return false;
        } else {
            if (HarvestUtil.getHarvestsCount(InventoryUtil.loadEntityInventory(creature)) == 0)
                return false;
            localVillage = creature.world.getVillageCollection().getNearestVillage(new BlockPos(creature), 40);
            return searchForDestination();
        }
    }

    public boolean shouldContinueExecuting() {
        int harvestsCount = HarvestUtil.getHarvestsCount(InventoryUtil.loadEntityInventory(creature));
        return harvestsCount > 0 || (runDelay > 0 && harvestsCount == 0);
    }

    public void startExecuting() {
        double centerX = destinationBlock.getX() + 0.5D;
        double y = destinationBlock.getY() + 1;
        double centerZ = destinationBlock.getZ() + 0.5D;

        creature.getNavigator().tryMoveToXYZ(centerX, y, centerZ, movementSpeed);
        timeoutCounter = 0;
    }

    public void updateTask() {
        double dist = Math.sqrt(creature.getPosition().add(0.5d, 0.5d, 0.5d).distanceSq(destinationBlock));
        if (dist > 1.2 && runDelay == 0) {
            localInv = null;
            timeoutCounter++;

            if (timeoutCounter % 2 == 0) {
                double x = destinationBlock.getX() + 0.8d;
                double y = destinationBlock.getY() + (failureStreak % 2 == 1 ? 0.5d : 1);
                double z = destinationBlock.getZ() + 0.5d;

//                ((PathNavigateGround) creature.getNavigator()).setEnterDoors(false);
                Path path = creature.getNavigator().getPathToPos(destinationBlock);

                if (path != null)
                    failureStreak = 0;
                else failureStreak++; //minecraft's pathfinder in our hearts

                if (failureStreak >= 10)
                    searchForDestination();

                creature.getNavigator().tryMoveToXYZ(x, y, z, movementSpeed);
            }
        } else {
            if (localInv == null)
                localInv = InventoryUtil.loadEntityInventory(creature);


            if (runDelay <= 0) { //stage 1 - door opening
                BlockPos doorPos = destinationDoor;
                Block door = creature.world.getBlockState(doorPos).getBlock();
                if (door == Blocks.OAK_DOOR)
                    ((BlockDoor) door).toggleDoor(creature.world, doorPos, true);
            }

            if (runDelay < 10) { //stage 2 - zombie position correction
                double x = destinationBlock.down().getX() + 0.5d;
                double y = destinationBlock.down().getY() + 0.5d;
                double z = destinationBlock.down().getZ() + 0.5d;
                creature.getNavigator().tryMoveToXYZ(x, y, z, movementSpeed);
            } else if (runDelay == 10) { //stage 3 - taking the crop
                creature.getNavigator().clearPath();
                creature.setHeldItem(EnumHand.MAIN_HAND, localInv.peek());
            }

            if (runDelay > 10 && runDelay <= 30) { //stage 4 - zombie rotation correction
                BlockPos doorPos = destinationDoor;
                EnumFacing facing = BlockDoor.getFacing(creature.world, doorPos);
                creature.rotationYaw = facing.getHorizontalAngle();
            }

            if (runDelay == 20) { //stage 5 - dropping item
                ItemStack stack = localInv.pop();

                double y = creature.posY - 0.3d + creature.getEyeHeight();

                double xDelta = destinationBlock.getX() + 0.5d - creature.posX;
                double zDelta = destinationBlock.getZ() + 0.5d - creature.posZ;
                EntityItem entityitem = new EntityItem(creature.world, creature.posX + xDelta / 2d, y, creature.posZ + zDelta / 2d, stack);

                float yaw = creature.rotationYawHead;
                float pitch = creature.rotationPitch;

                final float RAD = (float) (Math.PI / 180d);
                entityitem.motionX = -MathHelper.sin(yaw * RAD) * MathHelper.cos(pitch * RAD) * 0.3F;
                entityitem.motionZ = MathHelper.cos(yaw * RAD) * MathHelper.cos(pitch * RAD) * 0.3F;
                entityitem.motionY = -MathHelper.sin(pitch * RAD) * 0.3F + 0.05F;
                entityitem.setDefaultPickupDelay();

                creature.world.spawnEntity(entityitem);
                creature.setHeldItem(EnumHand.MAIN_HAND, ItemStack.EMPTY);

                saveEntityInventory(localInv);
            }

            runDelay++;
            if (runDelay >= 40) { //stage 6 - door closing
                BlockPos doorPos = destinationDoor;
                Block door = creature.world.getBlockState(doorPos).getBlock();
                if (door == Blocks.OAK_DOOR)
                    ((BlockDoor) door).toggleDoor(creature.world, doorPos, false);
                searchForDestination();
                runDelay = 0;
            }
        }
    }


    private void nextDoor() {
        NBTTagCompound data = creature.getEntityData();

        int currentDoor = data.getInteger("currentDoor") % localVillage.getNumVillageDoors();
        data.setInteger("currentDoor", currentDoor + 1);
        this.currentDoor = currentDoor;
    }

    private boolean searchForDestination() {
        if (localVillage == null) return false;

        nextDoor(); //<<<

        BlockPos dest = localVillage.getVillageDoorInfoList().get(currentDoor).getDoorBlockPos();
        EnumFacing facing = BlockDoor.getFacing(creature.world, dest);
        destinationBlock = dest.offset(facing.getOpposite());
        destinationDoor = dest;
        return true;
    }
}