package ru.whitewarrior.aimod.handler;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIMoveThroughVillage;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAITasks;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import ru.whitewarrior.aimod.ai.EntityAIHarvestFarmlandUniversal;
import ru.whitewarrior.aimod.ai.EntityAIMoveToNextDoor;
import ru.whitewarrior.aimod.utils.HarvestUtil;
import ru.whitewarrior.aimod.utils.InventoryUtil;
import ru.whitewarrior.aimod.utils.QueueInventory;

import java.util.Set;

public class AiEventHandler {

    @SubscribeEvent
    public void event(LivingEvent.LivingUpdateEvent event){
        if(event.getEntity() instanceof EntityZombie && !event.getEntity().world.isRemote){
            EntityCreature creature = (EntityCreature) event.getEntity();

            Class<?> clazz = null;

            Set<EntityAITasks.EntityAITaskEntry> executingTaskEntries = creature.tasks.executingTaskEntries;
            for(EntityAITasks.EntityAITaskEntry t : executingTaskEntries)
                clazz = t.action.getClass();

            String res = "unknown";

            if(clazz == EntityAIMoveThroughVillage.class)
                res = "moving through village";
            else if(clazz == EntityAIHarvestFarmlandUniversal.class)
                res = "harvesting...";
            else if(clazz == EntityAIMoveToNextDoor.class) {
                QueueInventory inv = InventoryUtil.loadEntityInventory((EntityLiving) event.getEntity());

                int count = HarvestUtil.getHarvestsCount(inv);
                res = "moving to next door: " + count + " deliveries";
            }

            event.getEntity().setCustomNameTag(res);
        }
    }

    @SubscribeEvent
    public void event(EntityJoinWorldEvent event){
        if(event.getEntity() instanceof EntityZombie){
            EntityZombie zombie = (EntityZombie) event.getEntity();
            zombie.targetTasks.taskEntries.clear();
            zombie.tasks.taskEntries.clear();


            zombie.tasks.addTask(0, new EntityAISwimming(zombie));
            zombie.tasks.addTask(1, new EntityAIMoveToNextDoor(zombie, 1.4d));
            zombie.tasks.addTask(5, new EntityAIHarvestFarmlandUniversal(zombie, 1.5D));
            zombie.tasks.addTask(6, new EntityAIMoveThroughVillage(zombie, 1.0D, false));
        }
    }
}
