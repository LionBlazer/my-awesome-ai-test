package ru.whitewarrior.aimod.utils;

import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class HarvestUtil {
    public static boolean isHarvestingItem(ItemStack stack){
        Item i = stack.getItem();
        return i == Items.WHEAT || i == Items.POTATO || i == Items.CARROT || i == Items.BEETROOT;
    }


    public static int getHarvestsCount(QueueInventory iInventory) {
        int count = 0;
        for (ItemStack stack : iInventory)
            if (HarvestUtil.isHarvestingItem(stack))
                count += stack.getCount();
        return count;
    }

}
