package ru.whitewarrior.aimod.utils;

import net.minecraft.entity.EntityLiving;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class InventoryUtil {
    public static final String INV_TAG_NAME = "additional_inventory";

    public static NBTTagCompound serializeInventory(QueueInventory inv) {
        NBTTagCompound tag = new NBTTagCompound();

        tag.setInteger("size", inv.size());

        int i = 0;
        for (ItemStack stack : inv) {
            NBTTagCompound element = new NBTTagCompound();
            stack.writeToNBT(element);
            tag.setTag("element" + i++, element);
        }
        return tag;
    }

    public static QueueInventory deserializeInventory(NBTTagCompound tag) {
        int size = tag.getInteger("size");

        QueueInventory queueInventory = new QueueInventory();
        for (int i = 0; i < size; i++) {
            NBTTagCompound element = tag.getCompoundTag("element" + i);
            ItemStack stack = new ItemStack(element);
            queueInventory.addItemLast(stack);
        }
        return queueInventory;
    }

    public static QueueInventory loadEntityInventory(EntityLiving living) {
        NBTTagCompound data = living.getEntityData();
        if (!data.hasKey(INV_TAG_NAME))
            return new QueueInventory();

        return InventoryUtil.deserializeInventory(data.getCompoundTag(INV_TAG_NAME));
    }

}