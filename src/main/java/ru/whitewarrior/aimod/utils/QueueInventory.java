package ru.whitewarrior.aimod.utils;

import net.minecraft.item.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.Objects;

public class QueueInventory implements Iterable<ItemStack> {
    private Deque<ItemStack> stacks = new ArrayDeque<>();

    public void addItem(ItemStack stack) {
        stacks.addFirst(Objects.requireNonNull(stack));
    }

    public void addItemLast(ItemStack stack) {
        stacks.addLast(Objects.requireNonNull(stack));
    }

    public ItemStack peek() {
        return stacks.isEmpty() ? ItemStack.EMPTY : stacks.peekLast();
    }

    public ItemStack pop() {
        return stacks.isEmpty() ? ItemStack.EMPTY : stacks.removeLast();
    }

    public int size() {
        return stacks.size();
    }

    @NotNull
    @Override
    public Iterator<ItemStack> iterator() {
        return stacks.iterator();
    }
}
